﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandlingKeys : MonoBehaviour
{
    GameObject player;
    public Text message;

    public GameController gameController;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        HideUIMessages(message);
    }

    private void OnTriggerStay(Collider collision)
    {
        if (collision.gameObject.tag == "TreasureKey")
        {
            message.text = "Press 'P' to pickup the treasure key";
            ShowUIMessages(message);

            if (CheckPlayerInput(collision.gameObject))
            {
                HideUIMessages(message);
            }
        }
        else if (collision.gameObject.tag == "FenceKey")
        {
            message.text = "Press 'P' to pickup the fence key";
            ShowUIMessages(message);

            if (CheckPlayerInput(collision.gameObject))
            {
                HideUIMessages(message);
            }
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        HideUIMessages(message);
    }

    private void ShowUIMessages(Text message)
    {
        if (message.enabled == false)
        {
            message.enabled = true;
        }
    }

    private void HideUIMessages(Text message)
    {
        if (message.enabled == true)
        {
            message.enabled = false;
        }
    }

    private bool CheckPlayerInput(GameObject item)
    {
        if (Input.GetButtonDown("Pick"))
        {
            return gameController.TakeTheKey(item);
        }
        return false;
    }
}
