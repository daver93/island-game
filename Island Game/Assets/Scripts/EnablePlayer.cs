﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnablePlayer : MonoBehaviour
{
    public GameObject player;

    private void Start()
    {
        player.SetActive(false);
    }

    public void ActivatePlayerInScene()
    {
        this.gameObject.SetActive(false);
        player.SetActive(true);
    }
}
