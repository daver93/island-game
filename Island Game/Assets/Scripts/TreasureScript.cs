﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TreasureScript : MonoBehaviour
{
    public GameController gameController;
    public Text message;
    bool isOpen;

    public Animator chestAnimation;

    private void Start()
    {
        isOpen = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "TreasureChest")
        {
            if (isOpen)
            {
                message.enabled = false;
            }
            else
            {
                if (gameController.hasTreasureKey)
                {
                    message.text = "Press 'P' to open the chest";

                    OpenChest();
                }
                else
                {
                    message.text = "I need a key to open this chest";
                }
                message.enabled = true;
            }
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.tag == "TreasureChest")
        {
            message.enabled = false;
        }
    }

    private void OpenChest()
    {
        if (gameController.hasTreasureKey && !isOpen)
        {
            if (Input.GetButtonDown("Pick"))
            {
                chestAnimation.SetTrigger("OpenChest");
                isOpen = true;
            }
        }
    }
}
