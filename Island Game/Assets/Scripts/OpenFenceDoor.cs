﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenFenceDoor : MonoBehaviour
{
    public GameController gameController;

    public Animator openFenceDoorAnimation;

    public Text message;

    private bool isOpen;

    private void Start()
    {
        isOpen = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "FenceDoor" && !isOpen)
        {
            message.text = "Press 'O' to open fence door";
            message.enabled = true;
            if (gameController.hasFenceKey && Input.GetButtonDown("Open"))
            {
                isOpen = true;
                message.enabled = false;
                openFenceDoorAnimation.SetTrigger("open");
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "FenceDoor")
        {
            message.text = "";
            message.enabled = false;
        }
    }
}
