﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    public bool hasTreasureKey;
    public bool hasFenceKey;

    private void Start()
    {
        hasTreasureKey = false;
        hasFenceKey = false;
    }

    public bool TakeTheKey(GameObject key)
    {
        if (key.gameObject.tag == "TreasureKey")
        {
            hasTreasureKey = true;
            Destroy(key.gameObject);
            return true;
        }
        else if (key.gameObject.tag == "FenceKey")
        {
            hasFenceKey = true;
            Destroy(key.gameObject);
            return true;
        }
        return false;
    }
}
